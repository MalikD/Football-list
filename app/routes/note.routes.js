module.exports = function(app) {

    var notes = require('C:/Users/Kabylou/node-football/app/controllers/note.controller.js');

    //Création d'une nouvelle note 
    app.post('/notes', notes.create);

    // Récupération de toute les notes  
    app.get('/notes', notes.findAll);

    // Une seule note par son ID
    app.get('/notes/:noteId', notes.findOne);

    // Mise à jour d'une note par son ID
    app.put('/notes/:noteId', notes.update);

    // Supression d'une note par son ID
    app.delete('/notes/:noteId', notes.delete);
    }