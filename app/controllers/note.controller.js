var Note = require('C:/Users/Kabylou/node-football/app/models/note.model.js');

exports.create = function(req, res) {
    // Creation et sauvegarde d'une nouvelle note
    if(!req.body.content) {
        res.status(400).send({message: "Impossible d'ajouter une note vide"});
    }

    var note = new Note({title: req.body.title || "Note sans titre", content: req.body.content});

    note.save(function(err, data) {
        console.log(data);
        if(err) {
            console.log(err);
            res.status(500).send({message: "Erreur lors de la création de la note"});
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    // Avoir toute les notes de la DB
    Note.find(function(err, notes){
        if(err) {
            res.status(500).send({message: "Erreur lors de la récupération d'une note."});
        } else {
            res.send(notes);
        }
    });
};

exports.findOne = function(req, res) {
    // recuperation d'une note par son ID
    Note.findById(req.params.noteId, function(err, data) {
        if(err) {
            res.status(500).send({message: "Impossible de recuperer la note par son id " + req.params.noteId});
        } else {
            res.send(data);
        }
    });
};

exports.update = function(req, res) {
    // Mise à jour d'une note par son ID
    Note.findById(req.params.noteId, function(err, note) {
        if(err) {
            res.status(500).send({message: "Impossible de recuperer la note par son id " + req.params.noteId});
        }

        note.title = req.body.title;
        note.content = req.body.content;

        note.save(function(err, data){
            if(err) {
                res.status(500).send({message: "Impossible MaJ la note par son id " + req.params.noteId});
            } else {
                res.send(data);
            }
        });
    });
};

exports.delete = function(req, res) {
    // Supression d'une note par son ID
    Note.remove({_id: req.params.noteId}, function(err, data) {
        if(err) {
            res.status(500).send({message: "Impossible de supprimer la note par son id " + req.params.id});
        } else {
            res.send({message: "Note supprimé avec succés !"})
        }
    });
};