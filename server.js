var express = require('express');
var bodyParser = require('body-parser');

//Creation de l'appliaction en express
var app = express();
//Parsing des requêtes URL
app.use(bodyParser.urlencoded({extended: true}))
//Parsing des requêtes du json
app.use(bodyParser.json())


//configuration de la base de données 
var dbConfig = require('C:/Users/Kabylou/node-football/config/database.config.js');
var mongoose = require('mongoose');

mongoose.connect(dbConfig.url, {
	useMongoClient : true 
});

mongoose.connection.on('error', function()
{
	console.log('Connection impossible à la base de données ');
	process.exit();
});

mongoose.connection.once('open', function() 
{
	console.log("Connection réussie à la BDD ");
})





//Definition d'une route basique
app.get('/' , function(req,res){

res.json({"message": "Bienvenu sur l'application de listing de footballeurs"})

});



//Demande de route
require('./app/routes/note.routes.js')(app);


//écoute des requêtes 
app.listen(3000, function() {


	console.log("Serveur en écoute sur le port 3000");
});







